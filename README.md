## INSTRUCCIONES DE INSTALACION

## Requerimientos Basicos:
	- Tener instalado node.js en el sistema
	- Tener instalado node package managment(npm)
	- Tener instalado Angular cli. npm install -g @angular/cli
	- Tener instalado Ionic cli.  npm install -g ionic


Se debe realizar la instalación del servidor backend-node y de la aplicacion ionic.

### Instalacion de Backend-Node.
1. La aplicación se encuentra dentro de la carpeta: /backEnd-Node
2. Dentro de esta instalar las dependecias de node con el comando: npm install
3. Levantar el servidor con el comando: npm run start
4. El servidor se monta en el **puerto 3000**

### Instalacion de Ionic.
1. La aplicacion se encuentra dentro de la carpeta: /ionic-chat
2. Dentro de esta instalar las dependecias de node con el comando: npm install
3. Levantar el la aplicacion con el comando: ionic serve
4. El servidor se monta en el **puerto 8100**

### Uso de la apliacion.
1. Se presenta la aplicacion via navegador web en **http://localhost:8100** , el cual en las opciones de desarrollador
   se debe elegir vista como celular(Responsive Design)
2. La aplicacion muestra como primera pantalla el chat, que ya se encuentra configurado.
3. Se debe ir al tab de configuracion para guardar el nombre que se utilizara.
4. Se pude abrir la aplicacion en otro navegador con los mismos pasos, para emular el chat.

