import { Component, OnInit } from '@angular/core';
import { MensajesService } from 'src/app/services/mensajes.service';
import { ToastController } from "@ionic/angular";

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: []
})
export class Tab2Page implements OnInit {

  nombre= "";

  constructor(private msgService: MensajesService,
              private toastController: ToastController) { }

  ngOnInit() {
  }

  async guardar(){
    if( this.nombre.trim().length === 0 ){
      return;
    }

    this.msgService.emiteNickName(this.nombre);
    this.showToast("Nick modificado")
    
  }


  async showToast(msg){
    let toast = await this.toastController.create({
      message: msg,
      position: 'top',
      duration: 1000
    });
    toast.present();
  }
}
