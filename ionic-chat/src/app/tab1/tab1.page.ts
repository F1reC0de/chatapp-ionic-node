import { Component, OnInit } from '@angular/core';

import { ToastController } from "@ionic/angular";
import { MensajesService } from 'src/app/services/mensajes.service';
import { Socket } from 'ngx-socket-io';


@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit{
  message = '';
  messages = this.msgService.messages;
  currentUser = this.msgService.currentUser;
  nickName = this.msgService.nickName;

  constructor(
              private toastController: ToastController,
              private msgService: MensajesService,
              public socket: Socket
              ) {}

  ngOnInit(){
    
    this.msgService.fromUserChange().subscribe( data=>{
      const user = data['user'];
      if(data['event'] == 'disconnect'){
        this.showToast('User left: ' + user);
      }else{
        this.showToast('User joined: ' + user);
      }
    });

    this.msgService.fromUserChangeNick().subscribe( data=>{
      console.log(data);
      
    });


    this.msgService.fromEventMessage().subscribe( data=>{      
      this.msgService.saveMessage(data);

    });


  }

  sendMessage(){
    this.socket.emit('send-message', {text: this.message })
    this.message = '';
  }

  // ionViewWillLeave(){
  //   this.msgService.disconnectSocket();
  // }

  async showToast(msg){
    let toast = await this.toastController.create({
      message: msg,
      position: 'top',
      duration: 1300
    });
    toast.present();
  }

}
