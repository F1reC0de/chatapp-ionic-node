import { Injectable, OnInit } from '@angular/core';
import { Socket } from "ngx-socket-io";
@Injectable({
  providedIn: 'root'
})
export class MensajesService  {

  public messages:any = [];
  public currentUser: string = '';
  public nickName: string = '';


  constructor(public socket: Socket) { 
    console.log('  Servicio inicializado');
    
    this.socket.connect();

    // Generar Nombre y lo pone como nickName
    let name =  `User-${new Date().getTime()}`;
    this.emiteNombre(name);
    this.emiteNickName('');
  }


  emiteNombre(name: string){
    this.currentUser = name;
    this.socket.emit('set-name', name);
    
  }

  emiteNickName(nick: string){
    console.log('info');
    
    this.nickName = nick;
    this.socket.emit('set-nick', nick);
    
  }


  fromUserChange(){
    return this.socket.fromEvent('userChange');
  }

  fromEventMessage(){
   return this.socket.fromEvent('message')
  }

 fromUserChangeNick(){
   return this.socket.fromEvent('userChangeNick')
  }
  saveMessage(data: any){
    console.log('longitud: ' + this.messages.length);
    
    this.messages.push(data);
  }

  sendMessage(message){
    this.socket.emit('send-message', {text: message })
  }

  disconnectSocket(){
    this.socket.disconnect();
  }

}

