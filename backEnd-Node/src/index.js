///Core Modules
const path = require('path');
const http = require('http');

//Npm Modules
const express = require('express');
const socketio= require('socket.io');

//configure app expres
const app = express();
//Refactor for real time app.
const server = http.createServer(app);
const io = socketio(server);

const port = 3000;


io.on('connection', (socket)=>{

    console.log("New WebSocket connection");

    socket.on('set-name',(name)=>{
        console.log('set name' + name)
        socket.username = name;
        io.emit('userChange',{
           user: name,
           event: 'joined' 
        })
    });
    
    socket.on('set-nick',(nick)=>{
        console.log('set-nick' + nick)
        socket.nickname = nick;
        io.emit('userChangeNick',{
           nick: nick,
           event: 'changed-nick' 
        })
    });

    socket.on('disconnect',()=>{
        io.emit('userChange',{
            user: socket.username,
            event: 'disconnect'
        })
    });


    socket.on('send-message',(message)=>{
        console.log('emitiendo mensaje: ' + message);
        
        io.emit('message',{
            msg: message.text,
            user: socket.username,
            nickname: socket.nickname,
            created: new Date()
        })
    }) ; 

});

server.listen(port, ()=>{
    console.log(`The server is up on port ${port}`);
})

